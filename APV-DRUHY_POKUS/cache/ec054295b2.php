<?php
// source: meetings.latte

use Latte\Runtime as LR;

class Templateec054295b2 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['s'])) trigger_error('Variable $s overwritten in foreach on line 26');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Meetings list<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <div class="container">
        <a class="btn btn-success" href="<?php
		echo $router->pathFor("newMeeting");
?>">
            Add new meeting
        </a>
    </div>


    <div class="container mt-5">
        <table class="table table-stripped table-hover">
            <tr>
                <th>start</th>
                <th>description	</th>
                <th>duration</th>
                <th>Update</th>
                <th>delete</th>


            </tr>

<?php
		$iterations = 0;
		foreach ($schuzky as $s) {
?>
                <tr>
                    <td><?php echo LR\Filters::escapeHtmlText($s['start']) /* line 28 */ ?></td>
                    <td><?php echo LR\Filters::escapeHtmlText($s['description']) /* line 29 */ ?></td>
                    <td><?php echo LR\Filters::escapeHtmlText($s['duration']) /* line 30 */ ?></td>

                    <td>
                        <a href="<?php
			echo $router->pathFor("meeting_update");
			?>?id_meeting=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($s['id_meeting'])) /* line 33 */ ?>">
                            <button class="btn-sm btn-primary">
                                <span class="fa fa-edit"></span>
                            </button>
                        </a>
                    </td>

                    <td>
                        <form method="post" onsubmit="return confirm('Are you sure?')" action="<?php
			echo $router->pathFor("meeting_delete");
			?>?id_meeting=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($s['id_meeting'])) /* line 41 */ ?>">
                            <button class="btn-sm btn-danger">
                                <span class="fa fa-trash"></span>
                            </button>
                        </form>
                    </td>

          

                </tr>
<?php
			$iterations++;
		}
?>
        </table>
    </div>
<?php
	}

}
