<?php
// source: meetings-form.latte

use Latte\Runtime as LR;

class Templatedd86845afd extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<div class="container">
<?php
		if (isset($message)) {
			?>        <p><?php echo LR\Filters::escapeHtmlText($message) /* line 3 */ ?></p>
<?php
		}
?>
    <form method="post">
        <!-- Osoba -->
        <div class="form-row">
            <div class="col-sm-4 form-group">
                <label for="start">Start</label>
                <input class="form-control" type="text" name="start" placeholder="start"
                       required value="<?php echo LR\Filters::escapeHtmlAttr($formData['start']) /* line 11 */ ?>">
            </div>

            <div class="col-sm-4 form-group">
                <label for="description">description</label>
                <input class="form-control" type="text" name="description" placeholder="description" required
                       value="<?php echo LR\Filters::escapeHtmlAttr($formData['description']) /* line 17 */ ?>">
            </div>

            <div class="col-sm-4 form-group">
                <label for="duration">Duration</label>
                <input class="form-control" type="text" name="duration" placeholder="duration" required
                       value="<?php echo LR\Filters::escapeHtmlAttr($formData['duration']) /* line 23 */ ?>">
            </div>
        </div>

        

                <!-- Adresy -->
                <div class="form-row">
                    <div class="col-sm-4 form-group">
                        <label for="city">City</label>
                        <input class="form-control" type="text" name="city" placeholder="City" value="<?php
		echo LR\Filters::escapeHtmlAttr($formData['city']) /* line 33 */ ?>">
                     </div>
                     
                     <div class="col-sm-4 form-group">
                        <label for="street_name">Street name</label>
                        <input class="form-control" type="text" name="street_name" placeholder="Street name" value="<?php
		echo LR\Filters::escapeHtmlAttr($formData['street_name']) /* line 38 */ ?>">
                     </div>
                     
                     <div class="col-sm-2 form-group">
                        <label for="street_number">Street number</label>
                        <input class="form-control" type="number" name="street_number" placeholder="Street number" value="<?php
		echo LR\Filters::escapeHtmlAttr($formData['street_number']) /* line 43 */ ?>">
                     </div>
        
                     <div class="col-sm-2 form-group">
                        <label for="zip">Zip</label>
                        <input class="form-control" type="text" name="zip" placeholder="Street number" value="<?php
		echo LR\Filters::escapeHtmlAttr($formData['zip']) /* line 48 */ ?>">
                     </div>
                </div>




        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<?php
		return get_defined_vars();
	}

}
