<?php
// source: person-form.latte

use Latte\Runtime as LR;

class Templateac3a50b8c8 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<div class="container">
<?php
		if (isset($message)) {
			?>        <p><?php echo LR\Filters::escapeHtmlText($message) /* line 3 */ ?></p>
<?php
		}
?>
    <form method="post">
        <!-- Osoba -->
        <div class="form-row">
             <div class="col-sm-4 form-group">
                <label for="first_name">First nnnnnnname</label>
                <input class="form-control" type="text" name="first_name" placeholder="First name" 
                        required value="<?php echo LR\Filters::escapeHtmlAttr($formData['first_name']) /* line 11 */ ?>">
             </div>
             
             <div class="col-sm-4 form-group">
                <label for="last_name">Last name</label>
                <input class="form-control" type="text" name="last_name" placeholder="Last name" required
                value="<?php echo LR\Filters::escapeHtmlAttr($formData['last_name']) /* line 17 */ ?>">
             </div>
             
             <div class="col-sm-4 form-group">
                <label for="nickname">Nickname</label>
                <input class="form-control" type="text" name="nickname" placeholder="Nickname" required
                value="<?php echo LR\Filters::escapeHtmlAttr($formData['nickname']) /* line 23 */ ?>">
             </div>
        </div>
        
        <div class="form-row">
             <div class="col-sm-4 form-group">
                <label for="height">Height</label>
                <input class="form-control" type="number" name="height" placeholder="Height"
                value="<?php echo LR\Filters::escapeHtmlAttr($formData['height']) /* line 31 */ ?>">
             </div>
             
             <div class="col-sm-4 form-group">
                <label for="gender">Gender</label>
                <select class="form-control" name="gender">
                    <option value="female" <?php
		if ($formData['gender']=='female') {
			?>selected<?php
		}
?>>Female</option>
                    <option value="male" <?php
		if ($formData['gender']=='male') {
			?>selected<?php
		}
?>>Male</option>
                </select>                              
             </div>
             
             <div class="col-sm-4 form-group">
                <label for="birth_day">Birthday</label>
                <input class="form-control" type="date" name="birth_day" placeholder="Date of birth",
                value="<?php echo LR\Filters::escapeHtmlAttr($formData['birth_day']) /* line 45 */ ?>">
             </div>
        </div>
        <!-- Adresy -->
        <div class="form-row">
            <div class="col-sm-4 form-group">
                <label for="city">City</label>
                <input class="form-control" type="text" name="city" placeholder="City" value="<?php echo LR\Filters::escapeHtmlAttr($formData['city']) /* line 52 */ ?>">
             </div>
             
             <div class="col-sm-4 form-group">
                <label for="street_name">Street name</label>
                <input class="form-control" type="text" name="street_name" placeholder="Street name" value="<?php
		echo LR\Filters::escapeHtmlAttr($formData['street_name']) /* line 57 */ ?>">
             </div>
             
             <div class="col-sm-2 form-group">
                <label for="street_number">Street number</label>
                <input class="form-control" type="number" name="street_number" placeholder="Street number" value="<?php
		echo LR\Filters::escapeHtmlAttr($formData['street_number']) /* line 62 */ ?>">
             </div>

             <div class="col-sm-2 form-group">
                <label for="zip">Zip</label>
                <input class="form-control" type="text" name="zip" placeholder="Street number" value="<?php
		echo LR\Filters::escapeHtmlAttr($formData['zip']) /* line 67 */ ?>">
             </div>
        </div>


         <br>
         <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div><?php
		return get_defined_vars();
	}

}
