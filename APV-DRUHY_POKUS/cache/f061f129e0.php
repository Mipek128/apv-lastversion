<?php
// source: personProfil.latte

use Latte\Runtime as LR;

class Templatef061f129e0 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Person profil<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <td>
        <a href="<?php
		echo $router->pathFor("persons_update");
		?>?id_person=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($o['id_person'])) /* line 8 */ ?>">
            <button class="btn-sm btn-primary">
                <span class="fa fa-edit"></span>
            </button>
        </a>
    </td>

    <td>
        <form method="post" onsubmit="return confirm('Are you sure?')" action="<?php
		echo $router->pathFor("person_delete");
		?>?id_person=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($o['id_person'])) /* line 16 */ ?>">
            <button class="btn-sm btn-danger">
                <span class="fa fa-trash"></span>
            </button>
        </form>
    </td>

        <h2>Person profil</h2>
        <table>
            <tr>
                <th>Firstname:</th>
                <td><?php echo LR\Filters::escapeHtmlText(empty($o['first_name']) ? 'empty' : $o['first_name']) /* line 27 */ ?></td>
            </tr>
            <tr>
                <th>Lastname:</th>
                <td><?php echo LR\Filters::escapeHtmlText(empty($o['last_name']) ? 'empty' : $o['last_name']) /* line 31 */ ?></td>
            </tr>
            <tr>
                <th>Nickname:</th>
                <td><?php echo LR\Filters::escapeHtmlText(empty($o['nickname']) ? 'empty' : $o['nickname']) /* line 35 */ ?></td>
            </tr>
            <tr>
                <th>Birth day:</th>
                <td><?php echo LR\Filters::escapeHtmlText(empty($o['birth_day']) ? 'empty' : $o['birth_day']) /* line 39 */ ?></td>
            </tr>
            <tr>
                <th>Height:</th>
                <td><?php echo LR\Filters::escapeHtmlText(empty($o['height']) ? 'empty' : $o['height']) /* line 43 */ ?></td>
            </tr>
            <tr>
                <th>Gender:</th>
                <td><?php echo LR\Filters::escapeHtmlText(empty($o['gender']) ? 'empty' : $o['gender']) /* line 47 */ ?></td>
            </tr>
        </table>


<?php
	}

}
