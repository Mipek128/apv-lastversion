<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');


$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');


/* Vypis osob */
$app->get('/persons', function (Request $request, Response $response, $args) {
    $stmt = $this->db->query('SELECT *,(SELECT count(*) as countperson
FROM person) FROM person ORDER BY first_name');

    $tplVars['osoby'] = $stmt->fetchAll();
    $stmt = $this->db->query('SELECT count(*) as countPerson
FROM person');
    $tplVars['countPerson'] = $stmt->fetchAll();
    $stmt = $this->db->query('SELECT MAX(start) as lastMeeting
FROM meeting');
    $tplVars['lastMeeting'] = $stmt->fetchAll();
    return $this->view->render($response, 'persons.latte', $tplVars);
})->setName('persons');

/* ----------------------------------------------------*/
/*               Person profil search      nefungujee            */
/* ----------------------------------------------------*/
$app->post('/persons', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['find'])) {
        $tplVars['message'] = 'Fill required fields';
    }
        else{
            try{
                $stmt = $this->db->query("SELECT * FROM person WHERE first_name 
                                            LIKE $search OR last_name 
                                            LIKE $search OR nickname 
                                            LIKE $search OR height
                                            LIKE $search OR gender
                                            LIKE $search OR profil 
                                      ORDER BY first_name");
                $stmt->bindValue(':find', $params['find']);
                $stmt->execute();
            }catch (PDOException $e) {
                echo $e->getMessage();
            }
        }
    return $this->view->render($response, 'persons.latte', $tplVars);
})->setName('person_find');

/* Zobraz formular pre pridanie novej osoby */
$app->get('/persons/new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'first_name' => '',
        'last_name' => '',
        'nickname' => '',
        'id_location' => null,
        'gender' => '',
        'height' => '',
        'birth_day' => '',
        'city' => '',
        'street_name' => '',
        'street_number' => '',
        'zip' => ''
    ];
    return $this->view->render($response, 'newPerson.latte', $tplVars);
})->setName('newPerson');


function insert_location($db, $formData) {
    try {
        $stmt = $db->prepare("INSERT INTO location (city, street_name, street_number, zip) VALUES (:city, :street_name, :street_number, :zip)");
        $stmt->bindValue(":city", empty($formData['city']) ? null : $formData['city']);
        $stmt->bindValue(":street_number", empty($formData['street_number']) ? null : $formData['street_number']);
        $stmt->bindValue(":street_name", empty($formData['street_name']) ? null : $formData['street_name']);
        $stmt->bindValue(":zip", empty($formData['zip']) ? null : $formData['zip']);
        $stmt->execute();
        return $db->lastInsertId('location_id_location_seq');
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}



/* Odoslanie formulara a spracovanie dat */
$app->post('/persons/new', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) || !empty($formData['zip'])) {
            $id_location = insert_location($this->db, $formData);
        }
        try {
            $stmt = $this->db->prepare("INSERT INTO person (nickname, first_name, last_name, id_location, birth_day, height, gender)
									VALUES (:nickname, :first_name, :last_name, :id_location, :birth_day, :height, :gender)");

            $stmt->bindValue(":nickname", $formData['nickname']); //   \;DROP TABLE person\;
            $stmt->bindValue(":first_name", $formData['first_name']);
            $stmt->bindValue(":last_name", $formData['last_name']);
            $stmt->bindValue(":id_location", $id_location);
            $stmt->bindValue(":gender", empty($formData['gender']) ?  null : $formData['gender']);
            $stmt->bindValue(":height", !empty($formData['height']) ? $formData['height'] : null);
            $stmt->bindValue(":birth_day", !empty($formData['birth_day']) ? $formData['birth_day'] : null);

            $stmt->execute();
            $tplVars['message'] = 'Person succesfully inserted';
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error occured';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'newPerson.latte', $tplVars);

});


/* Editacia uzivatela */
$app->get('/persons/update', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams(); // persons/update?id_person=55&gender=male ===> $params = ['id_person' => '55', 'gender' => 'male' ]
   // $personID = $params['id_person'];
    if (empty($params['id_person'])) {
        exit('id person is missing');
    } else {
        $stmt = $this->db->prepare("SELECT * FROM person LEFT JOIN location ON (person.id_location = location.id_location) WHERE id_person = :id_person");
        $stmt->bindValue(":id_person", $params['id_person']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
        if (empty($tplVars['formData'])) {
            exit('person not found');
        } else {
            return $this->view->render($response, 'updatePerson.latte', $tplVars);
        }
    }
})->setName('persons_update');

/* ODESLÁNÍ DAT PO UPDATU UŽIVATELE  */
$app->post('/persons/update', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $params = $request->getQueryParams();

    if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname'])) {
        $tplVars['message'] = 'Fill required fields';}
    else {
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) || !empty($formData['zip'])) {
            $id_location = insert_location($this->db, $formData);
        }
            try {
                $stmt = $this->db->prepare("UPDATE person SET nickname=:nickname,
                                             first_name=:first_name, last_name=:last_name, 
                                            id_location=:id_location, birth_day=:birth_day,
                                             height=:height, gender=:gender WHERE id_person=:id_person");
                $stmt->bindValue(':id_person', $params['id_person']);
                $stmt->bindValue(":nickname", $formData['nickname']);
                $stmt->bindValue(":first_name", $formData['first_name']);
                $stmt->bindValue(":last_name", $formData['last_name']);
                $stmt->bindValue(":id_location", $id_location);
                $stmt->bindValue(":gender", empty($formData['gender']) ?  null : $formData['gender']);
                $stmt->bindValue(":height", !empty($formData['height']) ? $formData['height'] : null);
                $stmt->bindValue(":birth_day", !empty($formData['birth_day']) ? $formData['birth_day'] : null);

                $stmt->execute();
                $tplVars['message'] = 'Person succesfully udpated';
            } catch (PDOException $e){
                $tplVars['message'] = 'Sorry, error occured';
                $this->logger->error($e->getMessage());
            }
        }
        $tplVars['formData'] = $formData;
        return $this->view->render($response, 'updatePerson.latte', $tplVars);
    });


/* Mazanie osoby */
$app->post('/persons/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams(); // persons/update?id_person=55
    if (empty($params['id_person'])) {
        exit('id person is missing');
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM person WHERE id_person = :id_person");
            $stmt->bindValue(':id_person', $params['id_person']);
            $stmt->execute();
        } catch (PDOException $e) {
            $this->logger->info($e);
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('persons'));

})->setName('person_delete');

/* ----------------------------------------------------*/
/*               Person profil                         */
/* ----------------------------------------------------*/
$app->get('/persons/info', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();

    if (empty($params['id_person'])) {
        exit('id person is missing');
    } else {
        try {
    $stmt = $this->db->prepare("SELECT * FROM person WHERE id_person = :id_person");
    $stmt->bindValue(':id_person', $params['id_person']);
    $stmt->execute();
    $tplVars['o'] = $stmt->fetch();
        } catch (PDOException $e) {
            $this->logger->info($e);
        }
    }
    $stmt = $this->db->prepare("SELECT * FROM location WHERE id_location = :id_location");
    $stmt->bindValue(":id_location", $tplVars['o']['id_location']);
    $stmt->execute();
    $tplVars['l'] = $stmt->fetch();


    $stmt = $this->db->prepare("SELECT * FROM contact LEFT JOIN contact_type USING (id_contact_type)
                                WHERE id_person = :id_person ORDER BY (id_contact)");
    $stmt->bindValue(":id_person", $params['id_person']);
    $stmt->execute();
    $tplVars['kontakty'] = $stmt->fetchAll();


    $stmt = $this->db->prepare("SELECT * FROM person_meeting LEFT JOIN meeting USING (id_meeting)
                                WHERE id_person = :id_person ORDER BY (id_meeting)");
    $stmt->bindValue(":id_person", $params['id_person']);
    $stmt->execute();
    $tplVars['schuzky'] = $stmt->fetchAll();


    $stmt = $this->db->prepare("SELECT * FROM relation LEFT JOIN relation_type USING (id_relation_type)
                                WHERE id_person1 = :id_person OR id_person2 = :id_person");
    $stmt->bindValue(":id_person", $params['id_person']);
    $stmt->execute();
    $tplVars['vztahy'] = $stmt->fetchAll();

    return $this->view->render($response, 'personProfil.latte', $tplVars);
})->setName('personsProfil');
/* --------------------------*/
/*         MEETINGY          */
/* --------------------------*/
/* ----------------------------------------------------*/
/*               Meeting profil                        */
/* ----------------------------------------------------*/
$app->get('/meetings/info', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();

    if (empty($params['id_meeting'])) {
        exit('id meetings is missing');
    } else {
        try {
            $stmt = $this->db->prepare("SELECT * FROM meeting
                                LEFT JOIN location USING (id_location)
                                WHERE id_meeting = :id_meeting");
            $stmt->bindValue(':id_meeting', $params['id_meeting']);
            $stmt->execute();
            $tplVars['s'] = $stmt->fetch();
        } catch (PDOException $e) {
            $this->logger->info($e);
        }
    }

    $stmt = $this->db->prepare("SELECT * FROM person_meeting
                                LEFT JOIN person USING (id_person)
                                WHERE id_meeting = :id_meeting");
    $tplVars['per'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name");

    $tplVars['per2'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name");
    $stmt->bindValue(':id_meeting', $params['id_meeting']);
    $stmt->execute();
    $tplVars['ucastnici'] = $stmt->fetchAll();
    //$tplVars['id_person'] = '';

    return $this->view->render($response, 'meetingProfil.latte', $tplVars);
})->setName('meetingsProfil');
/* ----------------------------------------------------*/
/*         EDIT MEETINGU                               */
/* ----------------------------------------------------*/
$app->get('/meetings/update', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_meeting'])) {
        exit('id meeting is missing');
    } else {
        $stmt = $this->db->prepare("SELECT * FROM meeting LEFT JOIN location USING (id_location) WHERE id_meeting = :id_meeting");
        $stmt->bindValue(":id_meeting", $params['id_meeting']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
        if (empty($tplVars['formData'])) {
            exit('Meeting not found');
        } else {
            return $this->view->render($response, 'updateMeeting.latte', $tplVars);
        }
    }
})->setName('meeting_update');
/* ----------------------------------------------------*/
/*         ODESLÁNÍ UPDATU MEETINGU                    */
/* ----------------------------------------------------*/
$app->post('/meetings/update', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $params = $request->getQueryParams();

    if (empty($formData['start']) || empty($formData['description']) || empty($formData['duration'])) {
        $tplVars['message'] = 'Fill required fields';}
    else {
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) || !empty($formData['zip'])) {
            $id_location = insert_location($this->db, $formData);
        }
        try {
            $stmt = $this->db->prepare("UPDATE meeting SET start =:start,
                                             description=:description, duration =:duration , 
                                            id_location=:id_location WHERE id_meeting=:id_meeting");
            $stmt->bindValue(':id_meeting', $params['id_meeting']);
            $stmt->bindValue(":start", $formData['start']);
            $stmt->bindValue(":description", $formData['description']);
            $stmt->bindValue(":duration", $formData['duration']);
            $stmt->bindValue(":id_location", $id_location);

            $stmt->execute();
            $tplVars['message'] = 'Meeting succesfully udpated';
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error occured';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'updateMeeting.latte', $tplVars);
});
/* ----------------------------------------------------*/
/*         DELETE MEETINGU                             */
/* ----------------------------------------------------*/
$app->post('/meetings/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_meeting'])) {
        exit('id meeting is missing');
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM meeting WHERE id_meeting = :id_meeting");
            $stmt->bindValue(':id_meeting', $params['id_meeting']);
            $stmt->execute();
        } catch (PDOException $e) {
            $this->logger->info($e);
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('meetings'));
})->setName('meeting_delete');
/* ----------------------------------------------------*/
/*                 DELETE Member                        */
/* ----------------------------------------------------*/
                $app->post('/meetings/delete_member', function (Request $request, Response $response, $args) {
                    $params = $request->getQueryParams();
                    if (empty($params['id_meeting']) or empty($params['id_person'])) {
                        exit('id meeting or id person is missing');
                    } else {
                        try {
                            $stmt = $this->db->prepare("DELETE FROM person_meeting WHERE id_meeting = :id_meeting AND id_person = :id_person");
                            $stmt->bindValue(':id_meeting', $params['id_meeting']);
                            $stmt->bindValue(':id_person', $params['id_person']);
                            $stmt->execute();
                        } catch (PDOException $e) {
                            $this->logger->info($e);
                        }
                    }
                    return $response->withHeader('Location', $this->router->pathFor('meetingsProfil'));
                })->setName('memberDelete');
/* ----------------------------------------------------*/
/*                 add Member      NEFUNGUJE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!       */
/* ----------------------------------------------------*/
$app->post('/meetings/add_member', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_meeting']) or empty($params['id_person'])) {
        exit('id meeting or id person is missing');
    } else {
        try {
            $stmt = $this->db->prepare("INSERT INTO person_meeting (id_person, id_meeting) VALUES (:id_person, :id_meeting)");
            $stmt->bindValue(':id_meeting', $params['id_meeting']);
            $stmt->bindValue(':id_person', $params['id_person']);
            $stmt->execute();
        } catch (PDOException $e) {
            $this->logger->info($e);
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('meetingProfil'));
})->setName('memberAdd');
/* ----------------------------------------------------*/
/*         Výpis MEETINGU                              */
/* ----------------------------------------------------*/
$app->get('/meetings', function (Request $request, Response $response, $args) {
    $stmt = $this->db->query('SELECT *, 
(SELECT count(*) as countmeeting
FROM person_meeting WHERE person_meeting.id_meeting = meeting.id_meeting)
FROM meeting');
    $tplVars['schuzky'] = $stmt->fetchAll();
    return $this->view->render($response, 'meetings.latte', $tplVars);
})->setName('meetings');
/* ----------------------------------------------------*/
/*         Přidání MEETINGU                            */
/* ----------------------------------------------------*/
$app->get('/meetings/new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'start' => '',
        'description' => '',
        'duration' => '',
        'id_location' => null,
        'city' => '',
        'street_name' => '',
        'street_number' => '',
        'zip' => '',

    ];
    $tplVars['per'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name");
    return $this->view->render($response, 'newMeeting.latte', $tplVars);
})->setName('newMeeting');
/* ----------------------------------------------------*/
/*               Poslání MEETINGU                      */
/* ----------------------------------------------------*/
$app->post('/meetings/new', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['start']) || empty($formData['description']) ||
        (empty($formData['city']) && empty($formData['street_name']) && empty($formData['street_number']) && empty($formData['zip'])))
    {
        $tplVars['message'] = 'Fill required fields';
        $tplVars['formData'] = $formData;
    }
    else {
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) ||
            !empty($formData['street_number']) || !empty($formData['zip'])) {
            $id_location = insert_location($this->db, $formData);
        }
        try {
            $stmt = $this->db->prepare("INSERT INTO meeting (start, description, duration, id_location)
                                            VALUES (:start, :description, :duration, :id_location)");
            $stmt->bindValue(":start", $formData['start']);
            $stmt->bindValue(":description", $formData['description']);
            $stmt->bindValue(":duration", $formData['duration']);
            $stmt->bindValue(":id_location", $id_location);

            $stmt->execute();
            $tplVars['message'] = 'Meeting succesfully inserted';
        }
        catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error occurcccccced';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'newMeeting.latte', $tplVars);
});









/* ----------------------------------------------------*/
/*         Výpis RELATIONS                             */
/* ----------------------------------------------------*/
    $app->get('/relations', function (Request $request, Response $response, $args) {
        $stmt = $this->db->query('SELECT * FROM relation 
                                LEFT JOIN (SELECT id_person as id_person1, first_name as first_name1, last_name as last_name1 FROM person) as person1 ON relation.id_person1 = person1.id_person1
                                LEFT JOIN (SELECT id_person as id_person2, first_name as first_name2, last_name as last_name2 FROM person) as person2 ON relation.id_person2 = person2.id_person2
                                LEFT JOIN relation_type rt on relation.id_relation_type = rt.id_relation_type ORDER BY first_name1');
        $tplVars['vztahy'] = $stmt->fetchAll();
        return $this->view->render($response, 'relations.latte', $tplVars);
    })->setName('relations');
/* ----------------------------------------------------*/
/*         Přidání RELATIONS                           */
/* ----------------------------------------------------*/
$app->get('/relations/new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'id_person1' => '',
        'id_person2' => '',
        'description' => '',
        'id_relation_type' => null,
    ];

    $tplVars['person1'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name");
    $tplVars['person2'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name");
    $tplVars['relation_type'] = $this->db->query("SELECT * FROM relation_type ORDER BY id_relation_type");

    return $this->view->render($response, 'newVztah.latte', $tplVars);
})->setName('newVztah');
/* ----------------------------------------------------*/
/*         Poslání RELATIONS                             */
/* ----------------------------------------------------*/
$app->post('/relations/new', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['id_person1']) || empty($formData['id_person2'])
        || empty($formData['description']) || empty($formData['id_relation_type']) )
    {
        $tplVars['message'] = 'Fill required fields';
        $tplVars['formData'] = $formData;
    }
    else {
        try {
            $stmt = $this->db->prepare("INSERT INTO relation (id_person1, id_person2, description, id_relation_type)
                                            VALUES (:id_person1, :id_person2, :description, :id_relation_type)");
            $stmt->bindValue(":id_person1", $formData['id_person1']);
            $stmt->bindValue(":id_person2", $formData['id_person2']);
            $stmt->bindValue(":description", $formData['description']);
            $stmt->bindValue(":id_relation_type", $formData['id_relation_type']);
            $stmt->execute();
            $tplVars['message'] = 'Relation succesfully inserted';
        }
        catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error occurcccccced';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'newVztah.latte', $tplVars);
});
/* ----------------------------------------------------*/
/*               Relation profil                       */
/* ----------------------------------------------------*/
$app->get('/relations/info', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_relation'])) {
        exit('id relation is missing');
    } else {
        try {
            $stmt = $this->db->prepare("SELECT * FROM relation 
                                LEFT JOIN (SELECT id_person as id_person1, first_name as first_name1, last_name as last_name1 FROM person) as person1 ON relation.id_person1 = person1.id_person1
                                LEFT JOIN (SELECT id_person as id_person2, first_name as first_name2, last_name as last_name2 FROM person) as person2 ON relation.id_person2 = person2.id_person2
                                LEFT JOIN relation_type rt on relation.id_relation_type = rt.id_relation_type
                                WHERE id_relation = :id_relation ");
            $stmt->bindValue(':id_relation', $params['id_relation']);
            $stmt->execute();
            $tplVars['vztahy'] = $stmt->fetch();
        } catch (PDOException $e) {
            $this->logger->info($e);
        }
    }
    return $this->view->render($response, 'relationProfil.latte', $tplVars);
})->setName('relationProfil');
/* ----------------------------------------------------*/
/*               Delete Relation                       */
/* ----------------------------------------------------*/
$app->post('/relations/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams(); // persons/update?id_person=55
    if (empty($params['id_relation'])) {
        exit('id relation is missing');
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM relation WHERE id_relation = :id_relation");
            $stmt->bindValue(':id_relation', $params['id_relation']);
            $stmt->execute();
        } catch (PDOException $e) {
            $this->logger->info($e);
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('relations'));
})->setName('relation_delete');
/* ----------------------------------------------------*/
/*         Update RELATIONS                          NAKONEC NEBUDU IMPLEMENTOVAT */
/* ----------------------------------------------------
$app->get('/relations/update', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_relation'])) {
        exit('id_relation is missing');
    } else {
        $stmt = $this->db->prepare("SELECT * FROM relation 
                                    LEFT JOIN relation_type USING (id_relation_type)
                                    WHERE id_relation = :id_relation");
        $stmt->bindValue(":id_relation", $params['id_relation']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
        if (empty($tplVars['formData'])) {
            exit('Relation not found');
        } else {
            return $this->view->render($response, 'updateVztah.latte', $tplVars);
        }
    }
    $tplVars['person1'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name");
    $tplVars['person2'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name");
    $tplVars['relation_type'] = $this->db->query("SELECT * FROM relation_type ORDER BY id_relation_type");
})->setName('relation_update');
*/
/* ----------------------------------------------------*/
/*         poslání update RELATIONS         nefungujeeee !!!            */
/* ----------------------------------------------------
$app->post('/relations/update', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['id_person1']) || empty($formData['id_person2']))
    {
        $tplVars['message'] = 'Fill required fields';
        $tplVars['formData'] = $formData;


    }
    else {
        try {
            $stmt = $this->db->prepare("UPDATE relation SET id_person1 = :id_person1, id_person2 = :id_person2, 
                                            description = :description, id_relation_type = :id_relation_type
                                            WHERE id_relation = :id_relation");
            $stmt->bindValue(":id_person1", $formData['id_person1']);
            $stmt->bindValue(":id_person2", $formData['id_person2']);
            $stmt->bindValue(":duration", $formData['duration']);
            $stmt->bindValue(":id_relation_type", $id_relation_type);

            $stmt->execute();
            $tplVars['message'] = 'Meeting succesfully inserted';
        }
        catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error occurcccccced';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'newMeeting.latte', $tplVars);
});


*/














